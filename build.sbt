name := "backsum"

version := "0.1-SNAPSHOT"

scalaVersion := "2.12.0"

libraryDependencies ++= Seq(
    "com.github.scopt" %% "scopt" % "3.5.0",
    "org.xerial" % "sqlite-jdbc" % "3.14.2.1",
    "org.scalatest" %% "scalatest" % "3.0.0" % "test"
)
