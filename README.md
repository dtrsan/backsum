# backsum

Utility to verify integrity of files.

[![build status](https://gitlab.com/dtrsan/backsum/badges/master/build.svg)](https://gitlab.com/dtrsan/backsum/commits/master)
[![coverage report](https://gitlab.com/dtrsan/backsum/badges/master/coverage.svg)](https://gitlab.com/dtrsan/backsum/commits/master)

## Usage

```
backsum v0.1-SNAPSHOT
Usage: backsum [compute|verify|diff] [options] <args>...

The utility to verify integrity of files.

  --version                Print version and exit.
  --help                   Print this help.


Command: compute [options] <PATH>
Compute checksums of files for the given path
  -o, --output PATH        Output path to store backsum database. If not specified, file .backsum.db is created in current directory.
  --full                   Perform full computation. If not specified, incremental mode is used.
  -f, --force              Force the action.
  --include PATTERN        Include files matching regex PATTERN.
  --exclude PATTERN        Exclude files matching regex PATTERN.
  --verbose                Turn on verbose output.
  <PATH>                   The path.


Command: verify [options] <DATABASE> <PATH>
Read checksums from the backsum database and check them
  --include PATTERN        Include files matching regex PATTERN.
  --exclude PATTERN        Exclude files matching regex PATTERN.
  --verbose                Turn on verbose output.
  <DATABASE>               Backsum database.
  <PATH>                   The path.


Command: diff [options] <DATABASE1> <DATABASE2>
Compare two backsum databases file by file
  --verbose                Turn on verbose output.
  <DATABASE1>              first backsum database to compare.
  <DATABASE2>              second backsum databases to compare.

```

## Building instructions

### Build jar

To build jar execute in the project root directory:
```
sbt assembly
```

The jar will be created at target/scala-2.11/backsum-assembly-<VERSION>.jar.

### Run tests

To run tests execute in the project root directory:
```
sbt test
```

### Measure test coverage

To run measure test coverage execute in the project root directory:
```
sbt clean coverage test coverageReport
```

