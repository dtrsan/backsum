package teapot.backsum.db

import java.nio.file.Path
import java.sql.{DriverManager, PreparedStatement}

import teapot.backsum.db.tables.{FilesService, MetadataService}

object Database {

  def apply(f: Path): Database = new Database(f)

  val VERSION = 1

  object Table {
    val METADATA = "metadata"
    val FILES = "files"
  }

  object SQL {
    val metadataTable =
      s"""CREATE TABLE ${Table.METADATA} (
        | version  INT  NOT NULL,
        | locked   INT  NOT NULL
        |)
      """.stripMargin

    val filesTable =
      s"""CREATE TABLE ${Table.FILES} (
        | path     TEXT PRIMARY KEY,
        | checksum TEXT NOT NULL,
        | size     INT  NOT NULL,
        | modified INT  NOT NULL
        |)
      """.stripMargin
  }
}

class Database(file: Path) {

  private val db = DriverManager.getConnection("jdbc:sqlite:" + file.toString)
  if(isNewDatabase) {
    init()
  }

  val metadataService =  new MetadataService(this)
  val filesService =  new FilesService(this)

  private def isNewDatabase = tables().isEmpty

  private def init(): Unit = {
    val stmt = db.createStatement()
    stmt.addBatch(Database.SQL.metadataTable)
    stmt.addBatch(Database.SQL.filesTable)
    stmt.addBatch(s"INSERT INTO ${Database.Table.METADATA} VALUES (${Database.VERSION}, 0)")
    stmt.executeBatch()
    stmt.close()
  }

  def prepareStatement(sql: String): PreparedStatement = db.prepareStatement(sql)

  def close(): Unit = db.close()

  def tables(): Seq[String] = {
    val result = db.getMetaData.getTables(null, null, "%", scala.Array("TABLE"))
    var tables = Seq[String]()
    while (result.next()) {
      tables = tables.+:(result.getString("TABLE_NAME"))
    }
    tables
  }
}
