package teapot.backsum.db.tables

import java.sql.{ResultSet, SQLException}
import java.util.Date

import teapot.backsum.db.Database
import teapot.backsum.db.Database.Table
import teapot.backsum.models.FileMetadata

import scala.util.Try

object FilesService {
  def apply(db: Database): FilesService = new FilesService(db)
}

class FilesService(db: Database) {

  private def insertStmt() = db.prepareStatement(s"INSERT INTO ${Table.FILES}(path, checksum, size, modified) VALUES(?, ?, ?, ?)")
  private def selectStmt() = db.prepareStatement(s"SELECT path, checksum, size, modified FROM ${Table.FILES} WHERE path = ? LIMIT 1")
  private def updateStmt() = db.prepareStatement(s"UPDATE ${Table.FILES} SET checksum = ?, size = ?, modified = ? WHERE path = ?")
  private def selectAllStmt() = db.prepareStatement(s"SELECT path, checksum, size, modified FROM ${Table.FILES} ORDER BY path ASC")

  def get(path: String): Option[FileMetadata] = {
    val stmt = selectStmt()
    stmt.setString(1, path)
    val result = stmt.executeQuery()

    val fileMetadata = if(result.next()) {
      Some(convert(result))
    } else {
      None
    }

    stmt.close()
    fileMetadata
  }

  def getAll: Iterator[FileMetadata] = {
    val stmt = selectAllStmt()
    val result = stmt.executeQuery()
    Iterator.continually(result).takeWhile(_.next()).map(convert)
  }

  private def convert(result: ResultSet) = {
    FileMetadata(
      path = result.getString("path"),
      checksum = result.getString("checksum"),
      size = result.getLong("size"),
      modified = new Date(result.getLong("modified"))
    )
  }

  def save(metadata: FileMetadata): Boolean = {
    val stmt = insertStmt()
    stmt.setString(1, metadata.path)
    stmt.setString(2, metadata.checksum)
    stmt.setLong(3, metadata.size)
    stmt.setLong(4, metadata.modified.getTime)
    val isSuccessful = Try {
      stmt.executeUpdate() == 1
    } recover {
      // Not the best way to do it but Java SQLite wrapper re-throws the error "(1555) SQLITE_CONSTRAINT_PRIMARYKEY"
      // as "The prepared statement has been finalized".
      case ex:SQLException => false
    }

    stmt.close()
    isSuccessful.get
  }

  def update(metadata: FileMetadata): Boolean = {
    val stmt = updateStmt()
    stmt.setString(1, metadata.checksum)
    stmt.setLong(2, metadata.size)
    stmt.setLong(3, metadata.modified.getTime)
    stmt.setString(4, metadata.path)

    val isSuccessful = stmt.executeUpdate() == 1

    stmt.close()
    isSuccessful
  }
}
