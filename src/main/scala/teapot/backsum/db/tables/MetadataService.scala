package teapot.backsum.db.tables

import teapot.backsum.db.Database
import teapot.backsum.db.Database.Table

class MetadataService(db: Database) {

  def lock(): Boolean = lock(true)

  def unlock(): Boolean = lock(false)

  private def lock(value: Boolean) = {
    val stmt = db.prepareStatement(s"UPDATE ${Table.METADATA} SET locked = ? WHERE locked = ?")
    stmt.setBoolean(1, value)
    stmt.setBoolean(2, !value)
    val result = stmt.executeUpdate()
    stmt.close()
    result == 1
  }

  def isLocked: Boolean =
    db.prepareStatement(s"SELECT locked FROM ${Table.METADATA}").executeQuery().getBoolean("locked")

  def version: Int =
    db.prepareStatement(s"SELECT version FROM ${Table.METADATA}").executeQuery().getInt("version")
}
