package teapot.backsum.files

import java.nio.file.{Files, Path}
import java.nio.file.attribute.BasicFileAttributes
import java.util.function.BiPredicate

import teapot.backsum.utilities.{FileUtility, PrintUtility}
import scala.util.matching.Regex

class Finder(path: Path, include: Seq[Regex], exclude: Seq[Regex], verbose: Boolean) {

  val print = PrintUtility(verbose)

  def this(path: Path) {
    this(path, Seq(), Seq(), false)
  }

  def withPath(path: Path): Finder = new Finder(path, include, exclude, verbose)

  def withInclude(pattern: Regex): Finder = new Finder(path, include.:+(pattern), exclude, verbose)

  def withInclude(patterns: Seq[Regex]): Finder = new Finder(path, patterns, exclude, verbose)

  def withExclude(pattern: Regex): Finder = new Finder(path, include, exclude.:+(pattern), verbose)

  def withExclude(patterns: Seq[Regex]): Finder = new Finder(path, include, patterns, verbose)

  def withVerbose(verbose: Boolean): Finder = new Finder(path, include, exclude, verbose)

  def findAll(): Iterator[Path] = {
    import scala.collection.JavaConverters._
    Files.find(path, Int.MaxValue, matcher).iterator().asScala
  }

  private val matcher = new BiPredicate[Path, BasicFileAttributes] {
    override def test(p: Path, attrs: BasicFileAttributes): Boolean = attrs.isRegularFile && shouldProcess(p)
  }

  private val relativePath = FileUtility.relativePath(path)(_)

  private def shouldProcess(f: Path) = {
    val p = relativePath(f)
    isIncluded(p) && isNotExcluded(p)
  }

  private def isIncluded(p: String) = include.isEmpty || include.exists(_.findFirstIn(p).nonEmpty)

  private def isNotExcluded(p: String) = exclude.isEmpty || exclude.forall(_.findFirstIn(p).isEmpty)

}
