package teapot.backsum.files

import java.io.{BufferedInputStream, FileInputStream}
import java.nio.file.Path

import teapot.backsum.utilities.PrintUtility

import scala.util.Try

object Reader {

  val BUFFER_SIZE_8MB = 8*1048576

  def apply(file: Path): Reader = new Reader(file)

  def apply(file: Path, chunkSize: Int): Reader = new Reader(file, chunkSize)
}

class Reader(file: Path, chunkSize: Int) {

  val print = PrintUtility()

  def this(file: Path) = this(file, Reader.BUFFER_SIZE_8MB)

  private val reader = new BufferedInputStream(new FileInputStream(file.toFile), chunkSize*4)

  type Chunk = Array[Byte]

  private def truncate(buffer: Chunk, size: Int) = if (size < chunkSize) buffer.slice(0, size) else buffer

  def chunks(): Iterator[Chunk] = {
    val buffer = new Chunk(chunkSize)
    Iterator.continually {
      val read = reader.read(buffer)
      (read, truncate(buffer, read))
    }.takeWhile(_._1 != -1).map(_._2)
  }

  def close(): Unit = {
    Try(reader.close()).recover {
      case _ => print.error("Failed to close file: %s", file)
    }
  }
}
