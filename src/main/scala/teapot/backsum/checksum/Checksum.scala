package teapot.backsum.checksum

import java.security.MessageDigest.getInstance

import teapot.backsum.checksum.Checksum.Algorithm

object Checksum {

  sealed abstract class Algorithm(val name: String)
  case object MD5 extends Algorithm("MD5")
  case object SHA1 extends Algorithm("SHA-1")

  def apply(algorithm: Algorithm): Checksum = new Checksum(algorithm)
}

class Checksum(algorithm: Algorithm) {

  private val messageDigest = getInstance(algorithm.name)

  def update(bytes: Array[Byte]) : Unit = messageDigest.update(bytes)

  def digest(): Array[Byte] = messageDigest.digest()

  def digest(bytes: Array[Byte]): Array[Byte] = messageDigest.digest(bytes)

  def reset(): Unit = messageDigest.reset()
}
