package teapot.backsum.checksum

import java.nio.file.Path
import java.util.concurrent.CountDownLatch

import teapot.backsum.checksum.Checksum.{MD5, SHA1}
import teapot.backsum.commands.ComputeCommand.Checksums
import teapot.backsum.files.Reader

import scala.concurrent.Future

class ChecksumCalculator {

  val sha1 = Checksum(SHA1)
  val md5 = Checksum(MD5)

  def computeChecksums(f: Path, isRunning: Boolean): Option[Checksums] = {
    sha1.reset()
    md5.reset()
    val reader = Reader(f)
    reader.chunks().takeWhile(_ => isRunning).foreach { chunk =>
      import scala.concurrent.ExecutionContext.Implicits.global
      val latch = new CountDownLatch(2)
      Future(sha1.update(chunk)).onComplete(_ => latch.countDown())
      Future(md5.update(chunk)).onComplete(_ => latch.countDown())
      try {
        latch.await()
      } catch {
        case ex: InterruptedException => ()
      }
    }
    reader.close()

    if (isRunning) {
      Some(Checksums(md5 = md5.digest(), sha1 = sha1.digest()))
    } else {
      None
    }
  }

}
