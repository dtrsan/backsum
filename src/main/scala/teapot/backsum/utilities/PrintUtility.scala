package teapot.backsum.utilities

object PrintUtility {
  def apply(verboseOutput: Boolean = false): PrintUtility = new PrintUtility(verboseOutput)
}

class PrintUtility(verboseOutput: Boolean = false) {

  def info(text: String, xs: Any*): Unit = printf(text + "\n", xs:_*)
  def verbose(text: String, xs: Any*): Unit = if (verboseOutput) printf(text + "\n", xs:_*)
  def error(text: String, xs: Any*): Unit = Console.err.println(text.format(xs:_*))
}
