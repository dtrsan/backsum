package teapot.backsum.utilities

import java.util.Base64

object FormatUtility {

  def toHex(bytes: Array[Byte]): String = bytes.map("%02x" format _).mkString

  def toBase64(bytes: Array[Byte]): String = new String(Base64.getEncoder.encode(bytes), "UTF-8")

  def fromBase64(checksum: String): Array[Byte] = Base64.getDecoder.decode(checksum)

}
