package teapot.backsum.utilities

import teapot.backsum.utilities.FormatUtility.{fromBase64, toBase64}

object ChecksumUtility {

  def encode(md5: Array[Byte], sha1: Array[Byte]): String = toBase64(md5 ++ sha1)

  def decode(checksum: String): (Array[Byte], Array[Byte]) = fromBase64(checksum).splitAt(8)
}
