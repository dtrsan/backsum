package teapot.backsum.utilities

import java.nio.file.{FileSystems, Path, Paths}

object FileUtility {

  private val separator = FileSystems.getDefault.getSeparator

  def relativePath(path: Path)(f: Path): String = f.toString.replaceFirst(path + separator, "")

  def prettify(path: Path): Path = path.toAbsolutePath.normalize

  def toPath(path: String): Path = prettify(Paths.get(path))
}
