package teapot.backsum.exceptions

class BacksumException(msg: String) extends RuntimeException(msg)
class BacksumIllegalArgumentException(msg: String) extends BacksumException(msg)
