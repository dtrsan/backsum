package teapot.backsum.cli

import teapot.configuration.{ComputeMode, Config, DEFAULT_DATABASE_FILENAME, DiffMode, VERSION, VerifyMode}
import teapot.backsum.utilities.FileUtility.toPath

import scala.util.Try

object Parameters {
  def apply(): Parameters = new Parameters()
}

class Parameters {

  val parser = new scopt.OptionParser[Config]("backsum") {
    head("backsum", VERSION)
    note("The utility to verify integrity of files.\n")
    opt[Unit]("version") action { (_, c) =>
      c.copy(version = true)
    } text "Print version and exit."
    help("help") text "Print this help."

    note("\n")
    cmd("compute").action((_, c) => c.copy(mode = Some(ComputeMode)))
      .text("Compute checksums of files for the given path")
      .children(
        opt[String]('o', "output").valueName("PATH") action { (output, config) =>
          config.copy(database = toPath(output))
        } text "Output path to store backsum database. " +
          "If not specified, file " + DEFAULT_DATABASE_FILENAME + " is created in current directory.",

        opt[Unit]("full") action { (_, c) =>
          c.copy(fullChecksum = true)
        } text "Perform full computation. If not specified, incremental mode is used.",

        opt[Unit]('f', "force") action { (_, c) =>
          c.copy(force = true)
        } text "Force the action.",

        opt[String]("include").unbounded().valueName("PATTERN") action { (pattern, c) =>
          c.copy(include = c.include :+ pattern.r)
        } validate (p => if (Try(p.r).isSuccess) success else failure(s"Invalid include pattern: $p"))
          text "Include files matching regex PATTERN.",

        opt[String]("exclude").unbounded().valueName("PATTERN") action { (pattern, c) =>
          c.copy(exclude = c.exclude :+ pattern.r)
        } validate (p => if (Try(p.r).isSuccess) success else failure(s"Invalid exclude pattern: $p"))
          text "Exclude files matching regex PATTERN.",

        opt[Unit]("verbose") action { (_, c) => c.copy(verbose = true)
        } text "Turn on verbose output.",

        arg[String]("<PATH>").required().minOccurs(1).maxOccurs(1) action { (path, c) =>
          c.copy(path = toPath(path))
        } text "The path."
      )

    note("\n")
    cmd("verify").action((_, c) => c.copy(mode = Some(VerifyMode)))
      .text("Read checksums from the backsum database and check them")
      .children(
        opt[String]("include").unbounded().valueName("PATTERN") action { (pattern, c) =>
          c.copy(include = c.include :+ pattern.r)
        } validate (p => if (Try(p.r).isSuccess) success else failure(s"Invalid include pattern: $p"))
          text "Include files matching regex PATTERN.",

        opt[String]("exclude").unbounded().valueName("PATTERN") action { (pattern, c) =>
          c.copy(exclude = c.exclude :+ pattern.r)
        } validate (p => if (Try(p.r).isSuccess) success else failure(s"Invalid exclude pattern: $p"))
          text "Exclude files matching regex PATTERN.",

        opt[Unit]("verbose") action { (_, c) => c.copy(verbose = true)
        } text "Turn on verbose output.",

        arg[String]("<DATABASE>").required().minOccurs(1).maxOccurs(1) action { (path, c) =>
          c.copy(database = toPath(path))
        } text "Backsum database.",

        arg[String]("<PATH>").required().minOccurs(1).maxOccurs(1) action { (path, c) =>
          c.copy(path = toPath(path))
        } text "The path."
      )

    note("\n")
    cmd("diff").action((_, c) => c.copy(mode = Some(DiffMode)))
      .text("Compare two backsum databases file by file")
      .children(
        opt[Unit]("verbose") action { (_, c) => c.copy(verbose = true)
        } text "Turn on verbose output.",

        arg[String]("<DATABASE1>").required().minOccurs(1).maxOccurs(1) action { (path, c) =>
          c.copy(database = toPath(path))
        } text "first backsum database to compare.",

        arg[String]("<DATABASE2>").required().minOccurs(1).maxOccurs(1) action { (path, c) =>
          c.copy(database2 = toPath(path))
        } text "second backsum databases to compare."
      )
  }

  def parse(args: Seq[String]): Option[Config] = parser.parse(args, Config())

  def printUsage(): Unit = parser.showUsage()
}
