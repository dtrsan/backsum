package teapot.backsum

import teapot.backsum.cli.Parameters
import teapot.backsum.commands.{Command, ComputeCommand, DiffCommand, VerifyCommand}
import teapot.backsum.exceptions.BacksumException
import teapot.backsum.utilities.PrintUtility
import teapot.configuration.{ComputeMode, Config, DiffMode, VERSION, VerifyMode}

import scala.util.Try

object Backsum {

  val print = PrintUtility()
  val parameters = Parameters()

  def main(args: Array[String]): Unit = {
    parameters.parse(args) map buildCommand foreach { cmd =>
      val thread = new Thread(() => {
        Try(cmd.execute()).recover {
          case ex:BacksumException => print.error(ex.getMessage)
        }.isSuccess
      })

      sys.addShutdownHook({
        if (cmd.isRunning) {
          print.info("Shutting down... Please wait...")
          cmd.cancel()
        }
        thread.interrupt()
        thread.join()
      })

      thread.start()
    }
  }

  def buildCommand(config: Config): Command = config.mode match {
    case Some(ComputeMode) => ComputeCommand(config)
    case Some(VerifyMode) => VerifyCommand(config)
    case Some(DiffMode) => DiffCommand(config.database, config.database2, config.verbose)
    case None if config.version => simpleCommand(printVersion)
    case _ => simpleCommand(parameters.printUsage)
  }

  def simpleCommand(fn: () => Unit): Command = () => fn()

  def printVersion(): Unit = print.info(VERSION)
}
