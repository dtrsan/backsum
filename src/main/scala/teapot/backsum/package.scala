package teapot

import java.nio.file.Path

import teapot.backsum.utilities.FileUtility.toPath

import scala.util.matching.Regex

package object configuration {

  val VERSION = "v0.1-SNAPSHOT"

  val DEFAULT_DATABASE_FILENAME = ".backsum.db"

  sealed trait Mode
  case object ComputeMode extends Mode
  case object VerifyMode extends Mode
  case object DiffMode extends Mode

  case class Config(mode: Option[Mode] = None,
                    path: Path = toPath("."),
                    database: Path = toPath("."),
                    database2: Path = toPath("."),
                    fullChecksum: Boolean = false,
                    include: Seq[Regex] = Seq(),
                    exclude: Seq[Regex] = Seq(),
                    force: Boolean = false,
                    verbose: Boolean = false,
                    version: Boolean = false)
}
