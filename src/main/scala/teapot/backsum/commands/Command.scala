package teapot.backsum.commands

trait Command {

  @volatile private var _isRunning = true

  def isRunning: Boolean = _isRunning
  def cancel(): Unit = {
    _isRunning = false
  }

  def execute(): Unit
}
