package teapot.backsum.commands

import java.nio.file.Files.{getLastModifiedTime, isDirectory, isReadable, isWritable, notExists, size}
import java.nio.file.{Path, Paths}
import java.util.Date
import java.util.concurrent.CountDownLatch

import teapot.backsum.checksum.Checksum
import teapot.backsum.checksum.Checksum.{MD5, SHA1}
import teapot.backsum.commands.ComputeCommand.Checksums
import teapot.backsum.db.Database
import teapot.backsum.db.tables.FilesService
import teapot.backsum.exceptions.BacksumIllegalArgumentException
import teapot.backsum.files.{Finder, Reader}
import teapot.backsum.models.FileMetadata
import teapot.backsum.utilities.FormatUtility.toHex
import teapot.backsum.utilities.{ChecksumUtility, FileUtility, PrintUtility}
import teapot.configuration.{Config, DEFAULT_DATABASE_FILENAME}

import scala.concurrent.Future
import scala.util.matching.Regex

object ComputeCommand {

  case class Checksums(md5: Array[Byte], sha1: Array[Byte])

  def apply(config: Config): ComputeCommand =
    new ComputeCommand(
      path = config.path,
      outputPath = config.database,
      fullChecksum = config.fullChecksum,
      include = config.include,
      exclude = config.exclude,
      force = config.force,
      verbose = config.verbose
    )
}

class ComputeCommand(path: Path,
                     outputPath: Path,
                     fullChecksum: Boolean,
                     include: Seq[Regex],
                     exclude: Seq[Regex],
                     force: Boolean,
                     verbose: Boolean) extends Command {

  val print = PrintUtility(verbose)
  val sha1 = Checksum(SHA1)
  val md5 = Checksum(MD5)
  private val relativePath = FileUtility.relativePath(path)(_)

  val databasePath =
    if (isDirectory(outputPath)) {
      Paths.get(outputPath.toString, DEFAULT_DATABASE_FILENAME)
    } else {
      outputPath
    }

  override def execute(): Unit = {

    if (notExists(path)) {
      throw new BacksumIllegalArgumentException(s"The path '$path' does not exist.")
    }

    if (!isDirectory(path)) {
      throw new BacksumIllegalArgumentException(s"The '$path' is not a directory.")
    }

    if (notExists(databasePath) && notExists(databasePath.getParent)) {
      throw new BacksumIllegalArgumentException(
        s"Couldn't create database '${databasePath.getFileName}'. Directory '${databasePath.getParent}' does not exist.")
    }

    if (notExists(databasePath) && !isWritable(databasePath.getParent)) {
      throw new BacksumIllegalArgumentException(
        s"Couldn't create database '${databasePath.getFileName}'. No write permission for '${databasePath.getParent}'.")
    }

    val database = Database(databasePath)
    if (!database.metadataService.lock()) {
      database.close()
      throw new BacksumIllegalArgumentException(s"Couldn't acquire lock on database '$databasePath'.")
    }

    val files = new Finder(path)
      .withInclude(include)
      .withExclude(exclude)
      .withVerbose(verbose)
      .findAll()

    files.takeWhile(_ => isRunning).map(process(database.filesService, path)).foreach(_ => ())

    database.metadataService.unlock()
    database.close()
  }

  def process(filesService: FilesService, basePath: Path)(f: Path): Boolean = {
    if (!isReadable(f)) {
      print.error("Couldn't read file '%s'. No read permission.", f)
      return false
    }

    val fileRelativePath = relativePath(f)
    val existingFileMetadata = filesService.get(fileRelativePath)
    if (shouldComputeChecksums(existingFileMetadata, f)) {
      val checksums = computeChecksums(f)
      val fileMetadata = checksums.map(createFileMetadata(f, _))
      saveFileMetadata(filesService, fileMetadata, existingFileMetadata)
      printChecksums(fileRelativePath, checksums)
    } else {
      print.verbose("Skipping %s", fileRelativePath)
    }

    true
  }

  private def printChecksums(fileRelativePath: String, checksums: Option[Checksums]): Unit = {
    checksums.foreach { cs =>
      print.info("%s %s  %s", toHex(cs.md5), toHex(cs.sha1), fileRelativePath)
    }
  }

  def computeChecksums(f: Path): Option[Checksums] = {
    sha1.reset()
    md5.reset()
    val reader = Reader(f)
    reader.chunks().takeWhile(_ => isRunning).foreach { chunk =>
      import scala.concurrent.ExecutionContext.Implicits.global
      val latch = new CountDownLatch(2)
      Future(sha1.update(chunk)).onComplete(_ => latch.countDown())
      Future(md5.update(chunk)).onComplete(_ => latch.countDown())
      try {
        latch.await()
      } catch {
        case ex: InterruptedException => ()
      }
    }
    reader.close()

    if (isRunning) {
      Some(Checksums(md5 = md5.digest(), sha1 = sha1.digest()))
    } else {
      None
    }
  }

  def shouldComputeChecksums(fileMetadata: Option[FileMetadata], file: Path): Boolean =
    isRunning && (
      fullChecksum ||
      fileMetadata.isEmpty ||
      fileMetadata.get.modified.getTime != getLastModifiedTime(file).toMillis ||
      fileMetadata.get.size != size(file)
    )
  def shouldUpdateChecksums(fileMetadata: FileMetadata, existing: Option[FileMetadata]): Boolean =
      isRunning && (
        existing.isEmpty ||
        existing.get.modified.getTime != fileMetadata.modified.getTime ||
        existing.get.size != fileMetadata.size ||
        existing.get.checksum != fileMetadata.checksum
      )

  def createFileMetadata(f: Path, checksums: Checksums): FileMetadata =
    FileMetadata(
      path = relativePath(f),
      checksum = ChecksumUtility.encode(md5 = checksums.md5, sha1 = checksums.sha1),
      size = size(f),
      modified = new Date(getLastModifiedTime(f).toMillis)
    )

  private def saveFileMetadata(filesService: FilesService,
                               fileMetadata: Option[FileMetadata],
                               existingFileMetadata: Option[FileMetadata]): Unit = {
    if (fileMetadata.isDefined && shouldUpdateChecksums(fileMetadata.get, existingFileMetadata)) {
      saveFileMetadata(filesService)(fileMetadata.get, update = existingFileMetadata.isDefined)
    }
  }

  private def saveFileMetadata(filesService: FilesService)(fileMetadata: FileMetadata, update: Boolean) =
    if (update) {
      filesService.update(fileMetadata)
    } else {
      filesService.save(fileMetadata)
    }
}
