package teapot.backsum.commands

import java.nio.file.Files.{isRegularFile, notExists}
import java.nio.file.Path

import teapot.backsum.db.Database
import teapot.backsum.exceptions.BacksumIllegalArgumentException
import teapot.backsum.models.FileMetadata
import teapot.backsum.utilities.PrintUtility

import scala.annotation.tailrec

object DiffCommand {
  def apply(first: Path, second: Path, verbose: Boolean): DiffCommand = new DiffCommand(first, second, verbose)
}

class DiffCommand(first: Path, second: Path, verbose: Boolean) extends Command {

  val print = PrintUtility(verbose)

  override def execute(): Unit = {
    val firstDatabase = openDatabase(first)
    val secondDatabase = openDatabase(second)

    val it1 = firstDatabase.filesService.getAll
    val it2 = secondDatabase.filesService.getAll

    traverse(it1, it2)

    close(firstDatabase)
    close(secondDatabase)
  }

  private def close(database: Database) = {
    database.metadataService.unlock()
    database.close()
  }

  @tailrec
  private def traverse(it1: Iterator[FileMetadata], it2: Iterator[FileMetadata],
                       left: Option[FileMetadata] = None, right: Option[FileMetadata] = None): Unit = {
    if (isRunning) {
      (left, right) match {
        case (Some(l), Some(r)) =>
          val f = printDiff(l, r)
          traverse(it1, it2, f._1, f._2)
        case (Some(l), None) if it2.isEmpty =>
          printInFirst(l)
          traverse(it1, it2, next(it1), None)
        case (None, Some(r)) if it1.isEmpty =>
          printInSecond(r)
          traverse(it1, it2, None, next(it2))
        case (Some(l), None) =>
          traverse(it1, it2, Some(l), next(it2))
        case (None, Some(r)) =>
          traverse(it1, it2, next(it1), Some(r))
        case (None, None) if it1.hasNext || it2.hasNext =>
          traverse(it1, it2, next(it1), next(it2))
        case _ => // my work is done here
      }
    }
  }

  private def printInFirst(m: FileMetadata) = print.info(s"${m.path}: DB1")
  private def printInSecond(m: FileMetadata) = print.info(s"${m.path}: DB2")

  private def printDiff(l: FileMetadata, r: FileMetadata) = {
    if (l.path  < r.path) {
      printInFirst(l)
      (None, Some(r))
    } else if( l.path > r.path) {
      printInSecond(r)
      (Some(l), None)
    } else {
      if (l.checksum == r.checksum) {
        print.verbose(s"${l.path}: OK")
      } else {
        print.info(s"${l.path}: ERR")
      }
      (None, None)
    }
  }

  private def next(it: Iterator[FileMetadata]) = {
    if (it.hasNext) {
      Some(it.next())
    } else {
      None
    }
  }

  private def openDatabase(databasePath: Path) = {
    if (notExists(databasePath)) {
      throw new BacksumIllegalArgumentException(s"The database '$databasePath' does not exist.")
    }

    if (!isRegularFile(databasePath)) {
      throw new BacksumIllegalArgumentException(s"The path '$databasePath' is not a database file.")
    }

    val database = Database(databasePath)
    if (!database.metadataService.lock()) {
      database.close()
      throw new BacksumIllegalArgumentException(s"Couldn't acquire lock on database '$databasePath'.")
    }
    database
  }
}
