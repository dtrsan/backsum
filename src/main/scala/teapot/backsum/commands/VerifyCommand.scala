package teapot.backsum.commands

import java.nio.file.Files.{size, exists, isDirectory, notExists}
import java.nio.file.{Path, Paths}

import teapot.backsum.checksum.ChecksumCalculator

import scala.util.control.Breaks.break
import teapot.backsum.db.Database
import teapot.backsum.db.tables.FilesService
import teapot.backsum.exceptions.BacksumIllegalArgumentException
import teapot.backsum.files.Finder
import teapot.backsum.models.FileMetadata
import teapot.backsum.utilities.{ChecksumUtility, FileUtility, PrintUtility}
import teapot.configuration.Config

import scala.util.matching.Regex

object VerifyCommand {
  def apply(config: Config): VerifyCommand =
    new VerifyCommand(
      basePath = config.path,
      databasePath = config.database,
      include = config.include,
      exclude = config.exclude,
      verbose = config.verbose
    )
}

class VerifyCommand(basePath: Path,
                    databasePath: Path,
                    include: Seq[Regex],
                    exclude: Seq[Regex],
                    verbose: Boolean) extends Command {

  private val print = PrintUtility(verbose)

  override def execute(): Unit = {
    if (notExists(basePath)) {
      throw new BacksumIllegalArgumentException(s"The path '$basePath' does not exist.")
    }

    if (!isDirectory(basePath)) {
      throw new BacksumIllegalArgumentException(s"The '$basePath' is not a directory.")
    }

    if (notExists(databasePath) && notExists(databasePath.getParent)) {
      throw new BacksumIllegalArgumentException(
        s"Couldn't create database '${databasePath.getFileName}'. Directory '${databasePath.getParent}' does not exist.")
    }

    val database = Database(databasePath)
    if (!database.metadataService.lock()) {
      database.close()
      throw new BacksumIllegalArgumentException(s"Couldn't acquire lock on database '$databasePath'.")
    }

    val inDatabase: Iterator[FileMetadata] = database.filesService.getAll
    val onDisk: Iterator[Path] = new Finder(basePath)
      .withInclude(include)
      .withExclude(exclude)
      .withVerbose(verbose)
      .findAll()

    processDatabase(inDatabase)
    processDisk(onDisk, database.filesService)

    database.metadataService.unlock()
    database.close()
  }

  def processDatabase(rows: Iterator[FileMetadata]): Unit = {
    rows.foreach { row =>
      val path = Paths.get(basePath.toString, row.path)

      if (!doesExistOnDisk(path)) {
        print.info("%s exists only in database", path.toString)
        break
      }
      if (!areSizesEqual(path, row.size)) {
        print.info("File on disk %s size different", path.toString)
        break
      }
      if (!areChecksumsEqual(path, row.checksum)) {
        print.info("Checksum does not match for file %s", path.toString)
      }
    }
  }

  def processDisk(onDisk: Iterator[Path], fileService: FilesService): Unit = {
    onDisk.foreach(file => {
      val result: Option[FileMetadata]  = fileService.get(FileUtility.relativePath(basePath)(file))
      if(result.isEmpty) {
        print.info("%s exists only on disk", file.toAbsolutePath.toString )
      }
    })
  }

  def doesExistOnDisk(path: Path): Boolean = exists(path)

  def areSizesEqual(path: Path, sizeInDB: Long): Boolean = size(path) == sizeInDB

  def areChecksumsEqual(path: Path, checksumInDB: String): Boolean = {
    val checksumCalculator = new ChecksumCalculator()
    val checksumOption = checksumCalculator.computeChecksums(path, isRunning)
    checksumOption.forall(checksum => ChecksumUtility.encode(checksum.md5, checksum.sha1) == checksumInDB)
  }
}