package teapot.backsum.models

import java.util.Date

case class FileMetadata(path: String, checksum: String, size: Long, modified: Date)

case class DatabaseMetadata(version: Int, isLocked: Boolean)
