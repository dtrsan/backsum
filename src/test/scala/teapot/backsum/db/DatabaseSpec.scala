package teapot.backsum.db

import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import teapot.backsum.helpers.FilesHelper

class DatabaseSpec extends FlatSpec with Matchers with BeforeAndAfterAll {

  behavior of "Database"

  var db: Database = _
  val filesHelper = new FilesHelper("database")

  override def beforeAll(): Unit = {
    val databasePath = filesHelper.registerPath(filesHelper.path(filesHelper.createTempDir(), "backsum.db"))
    db = Database(databasePath)
  }

  override def afterAll(): Unit = {
    db.close()
    filesHelper.cleanup()
  }

  def isDatabaseLocked =
    db.prepareStatement("SELECT locked FROM metadata").executeQuery().getBoolean("locked")

  def version =
    db.prepareStatement("SELECT version FROM metadata").executeQuery().getInt("version")

  it must "create table metadata" in {
    db.tables() should contain ("metadata")
  }

  it must "create table files" in {
    db.tables() should contain ("files")
  }

  it must "not lock newly initialized database" in {
    isDatabaseLocked should be (false)
  }

  it must "set correct database version" in {
    version should be (1)
  }
}
