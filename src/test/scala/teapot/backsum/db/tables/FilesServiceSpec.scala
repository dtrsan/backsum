package teapot.backsum.db.tables

import java.util.Date

import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import teapot.backsum.db.Database
import teapot.backsum.helpers.FilesHelper
import teapot.backsum.models.FileMetadata

class FilesServiceSpec extends FlatSpec with Matchers with BeforeAndAfterEach {

  behavior of "FilesService"

  var db: Database = _
  var filesService: FilesService = _

  val metadata1 = FileMetadata(
    path = "file1",
    checksum = "UUnUAwCaE5x+CFQF73YuGji+fRuYHy+2pKCgUkU/iHNz3B/o",
    size = 5,
    modified = new Date(1477052189938L)
  )

  val metadata2 = FileMetadata(
    path = "dir1/file2",
    checksum = "0DTjMKMnnF1eiRR7LYl04DlLQ1gbePqnzfK3bY4GyHdXKL0D",
    size = 10,
    modified = new Date(1477052079219L)
  )

  val metadata3 = FileMetadata(
    path = "dir1/file3",
    checksum = "0DTjMKMnnF1eiRR7LYl04DlLQ1gbePqnzfK3bY4GyHdXKL0D",
    size = 15,
    modified = new Date(1477052079219L)
  )

  val filesHelper = new FilesHelper("files-service")

  override def beforeEach() {
    val databasePath = filesHelper.registerPath(filesHelper.path(filesHelper.createTempDir(), "backsum.db"))
    db = Database(databasePath)

    val insert = db.prepareStatement(s"INSERT INTO files(path, checksum, size, modified) VALUES(?, ?, ?, ?)")
    insert.setString(1, metadata1.path)
    insert.setString(2, metadata1.checksum)
    insert.setLong(3, metadata1.size)
    insert.setLong(4, metadata1.modified.getTime)
    insert.execute()
    insert.clearParameters()
    insert.setString(1, metadata2.path)
    insert.setString(2, metadata2.checksum)
    insert.setLong(3, metadata2.size)
    insert.setLong(4, metadata2.modified.getTime)
    insert.execute()
    insert.clearParameters()
    insert.setString(1, metadata3.path)
    insert.setString(2, metadata3.checksum)
    insert.setLong(3, metadata3.size)
    insert.setLong(4, metadata3.modified.getTime)
    insert.execute()
    insert.close()

    filesService = FilesService(db)
  }

  override def afterEach() {
    db.close()
    filesHelper.cleanup()
  }

  it must "get existing file" in {
    val metadata = filesService.get("file1")

    metadata.isDefined should be (true)
    metadata.get.path should be (metadata1.path)
    metadata.get.checksum should be (metadata1.checksum)
    metadata.get.size should be (metadata1.size)
    metadata.get.modified should be (metadata1.modified)
  }

  it must "not get non-existing file" in {
    val metadata = filesService.get("non-existing")

    metadata.isEmpty should be (true)
  }

  it must "insert new file" in {
    val metadata = FileMetadata(
      path = "dummy-file",
      checksum = "dummy-checksum",
      size = 255,
      modified = new Date(120000L)
    )
    val isSaved = filesService.save(metadata)
    isSaved should be (true)

    verifyFileMetadata(metadata)
  }

  it must "not insert the same file twice" in {
    val metadata = FileMetadata(
      path = "dummy-file1",
      checksum = "dummy-checksum1",
      size = 511,
      modified = new Date(180000L)
    )
    filesService.save(metadata) should be (true)
    filesService.save(metadata) should be (false)
    filesService.save(metadata) should be (false)
    filesService.save(metadata) should be (false)
  }

  it must "update existing file" in {
    val metadata = FileMetadata(
      path = "file1",
      checksum = "dummy-checksum",
      size = 255,
      modified = new Date(120000L)
    )

    val isUpdated = filesService.update(metadata)
    isUpdated should be (true)

    verifyFileMetadata(metadata)
  }

  it must "not update non-existing file" in {
    val metadata = FileMetadata(
      path = "non-existing",
      checksum = "dummy-checksum",
      size = 255,
      modified = new Date(120000L)
    )

    val isUpdated = filesService.update(metadata)
    isUpdated should be (false)
  }

  it must "retrieve all data from FILES table" in {
    val files = filesService.getAll.toList
    files.length should be(3)

    files(0).path should be(metadata2.path)
    files(0).checksum should be(metadata2.checksum)
    files(0).size should be(metadata2.size)
    files(0).modified should be(metadata2.modified)

    files(1).path should be(metadata3.path)
    files(1).checksum should be(metadata3.checksum)
    files(1).size should be(metadata3.size)
    files(1).modified should be(metadata3.modified)

    files(2).path should be(metadata1.path)
    files(2).checksum should be(metadata1.checksum)
    files(2).size should be(metadata1.size)
    files(2).modified should be(metadata1.modified)
  }

  it must "get all files" in {
    val files = filesService.getAll.toSeq

    files.length should be(3)
    files.head.path should be("dir1/file2")
    files.last.path should be("file1")
  }

  def verifyFileMetadata(expected: FileMetadata) = {
    val stmt = db.prepareStatement(s"SELECT path, checksum, size, modified FROM files WHERE path = '${expected.path}'")
    val actual = stmt.executeQuery()
    actual.getString("path") should be (expected.path)
    actual.getString("checksum") should be (expected.checksum)
    actual.getLong("size") should be (expected.size)
    actual.getLong("modified") should be (expected.modified.getTime)
  }
}
