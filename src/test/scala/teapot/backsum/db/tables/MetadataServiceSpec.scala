package teapot.backsum.db.tables

import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import teapot.backsum.db.Database
import teapot.backsum.helpers.FilesHelper

class MetadataServiceSpec extends FlatSpec with Matchers  with BeforeAndAfterEach {

  behavior of "MetadataService"

  var db: Database = _
  val filesHelper = new FilesHelper("metadata-service")

  override def beforeEach(): Unit = {
    val databasePath = filesHelper.registerPath(filesHelper.path(filesHelper.createTempDir(), "backsum.db"))
    db = Database(databasePath)
  }

  override def afterEach(): Unit = {
    db.close()
    filesHelper.cleanup()
  }

  def updateVersion(version: Int) = {
    val stmt = db.prepareStatement("UPDATE metadata SET version = ?")
    stmt.setInt(1, version)
    stmt.executeUpdate()
  }

  def lockDatabase() = db.prepareStatement("UPDATE metadata SET locked = 1").executeUpdate()

  def unlockDatabase() = db.prepareStatement("UPDATE metadata SET locked = 0").executeUpdate()

  def isDatabaseLocked =
    db.prepareStatement("SELECT locked FROM metadata").executeQuery().getBoolean("locked")

  it must "fetch correct version" in {
    db.metadataService.version should be (1)
    updateVersion(5)
    db.metadataService.version should be (5)
    updateVersion(10)
    db.metadataService.version should be (10)
  }

  it must "lock unlocked database" in {
    unlockDatabase()
    db.metadataService.lock() should be (true)
    isDatabaseLocked should be (true)
  }

  it must "not lock already locked database" in {
    lockDatabase()
    db.metadataService.lock() should be (false)
    isDatabaseLocked should be (true)
  }

  it must "unlock locked database" in {
    lockDatabase()
    db.metadataService.unlock() should be (true)
    isDatabaseLocked should be (false)
  }

  it must "not unlock already unlocked database" in {
    unlockDatabase()
    db.metadataService.unlock() should be (false)
    isDatabaseLocked should be (false)
  }

  it must "detect database is locked" in {
    lockDatabase()
    db.metadataService.isLocked should be (true)
  }

  it must "detect database is unlocked" in {
    unlockDatabase()
    db.metadataService.isLocked should be (false)
  }
}
