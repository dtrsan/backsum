package teapot.backsum.files

import java.nio.file.Path

import org.scalatest.{FlatSpec, Matchers, BeforeAndAfterAll}
import teapot.backsum.helpers.FilesHelper

class FinderSpec extends FlatSpec with Matchers with BeforeAndAfterAll {

  behavior of "Finder"

  val directories = Seq(
    "directory1",
    "directory1/subDirectory1",
    "directory1/subDirectory2",
    "directory2",
    "directory2/subDirectory1",
    "directory2/subDirectory2",
    "directory3",
    "directory3/emptySubDirectory1",
    "directory3/subDirectoryWithSymlinks"
  )

  val files = Seq(
    "directory1/subDirectory1/file1",
    "directory1/subDirectory1/file2",
    "directory1/subDirectory2/file1",
    "directory1/subDirectory2/file2",
    "directory2/subDirectory1/file1",
    "directory2/subDirectory1/file2",
    "directory2/subDirectory2/file1",
    "directory2/subDirectory2/file2"
  )

  val symlinks = Seq(
    ("directory3/subDirectoryWithSymlinks/symlink1", "directory1"),
    ("directory3/subDirectoryWithSymlinks/symlink2", "directory2")
  )

  var basePath: Path = _

  val filesHelper = new FilesHelper("finder")

  override def beforeAll(): Unit = {
    basePath = filesHelper.createTempDir()

    directories.foreach { dir =>
      filesHelper.createDir(filesHelper.path(basePath, dir))
    }

    files.foreach { f =>
      filesHelper.createFile(filesHelper.path(basePath, f), f)
    }

    symlinks.foreach { l =>
      filesHelper.createSymLink(filesHelper.path(basePath, l._1), filesHelper.path(basePath, l._2))
    }
  }

  override def afterAll(): Unit = {
    filesHelper.cleanup()
  }

  private def relativePath(f: Path) = f.toString.replaceFirst(basePath + "/", "")

  it must "find all files" in {

    val expected = Set(
      "directory1/subDirectory1/file1",
      "directory1/subDirectory1/file2",
      "directory1/subDirectory2/file1",
      "directory1/subDirectory2/file2",
      "directory2/subDirectory1/file1",
      "directory2/subDirectory1/file2",
      "directory2/subDirectory2/file1",
      "directory2/subDirectory2/file2"
    )
    val actual = new Finder(basePath).findAll().map(relativePath).toSet

    actual should be (expected)
  }

  it must "find all files that match include pattern" in {

    val expected = Set[String](
      "directory1/subDirectory1/file1",
      "directory1/subDirectory2/file1",
      "directory2/subDirectory1/file1",
      "directory2/subDirectory2/file1"
    )
    val actual = new Finder(basePath).withInclude("file1".r).findAll().map(relativePath).toSet

    actual should be (expected)
  }

  it must "find all files that match multiple include patterns" in {

    val expected = Set[String](
      "directory1/subDirectory1/file1",
      "directory1/subDirectory1/file2",
      "directory1/subDirectory2/file1",
      "directory2/subDirectory1/file1",
      "directory2/subDirectory1/file2",
      "directory2/subDirectory2/file1"
    )
    val actual = new Finder(basePath).withInclude("file1".r).withInclude("subDirectory1".r).findAll()
      .map(relativePath).toSet

    actual should be (expected)
  }

  it must "not find files that match exclude pattern"  in  {

    val expected = Set[String](
      "directory1/subDirectory1/file2",
      "directory1/subDirectory2/file2",
      "directory2/subDirectory1/file2",
      "directory2/subDirectory2/file2"
    )
    val actual = new Finder(basePath).withExclude("file1".r).findAll().map(relativePath).toSet

    actual should be (expected)
  }

  it must "not find files that match multiple exclude patterns"  in  {

    val expected = Set[String](
      "directory1/subDirectory2/file2",
      "directory2/subDirectory2/file2"
    )
    val actual = new Finder(basePath).withExclude("file1".r).withExclude("subDirectory1".r)
      .findAll().map(relativePath).toSet

    actual should be (expected)
  }

  it must "not find files that match the same include and exclude patterns"  in  {

    val expected = Set[String]()
    val actual = new Finder(basePath).withInclude("file1".r).withExclude("file1".r).findAll().map(relativePath).toSet

    actual should be (expected)
  }

  it must "find files that match include and not match exclude pattern"  in  {

    val expected = Set[String](
      "directory1/subDirectory1/file1",
      "directory2/subDirectory1/file1"
    )
    val actual = new Finder(basePath).withInclude("file1".r).withExclude("subDirectory2".r)
      .findAll().map(relativePath).toSet

    actual should be (expected)
  }

  it must "find files that match overriden include patterns" in {

    val expected = Set[String](
      "directory1/subDirectory1/file2",
      "directory1/subDirectory2/file1",
      "directory1/subDirectory2/file2",
      "directory2/subDirectory1/file2",
      "directory2/subDirectory2/file1",
      "directory2/subDirectory2/file2"
    )
    val actual = new Finder(basePath) .withInclude("file1".r).withInclude(Seq("file2".r, "subDirectory2".r))
      .findAll().map(relativePath).toSet

    actual should be (expected)
  }

  it must "find files that match overriden exclude patterns" in {

    val expected = Set[String](
      "directory1/subDirectory2/file2",
      "directory2/subDirectory2/file2"
    )
    val actual = new Finder(basePath).withExclude("file2".r).withExclude(Seq("file1".r, "subDirectory1".r))
      .findAll().map(relativePath).toSet

    actual should be (expected)
  }
}
