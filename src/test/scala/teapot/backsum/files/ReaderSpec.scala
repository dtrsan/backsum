package teapot.backsum.files

import java.nio.file.Path

import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import teapot.backsum.helpers.FilesHelper

class ReaderSpec extends FlatSpec with Matchers with BeforeAndAfterAll  {

  behavior of "Reader"

  var file: Path = _
  val message = "The quick brown fox jumps over the lazy dog"

  val filesHelper = new FilesHelper("reader")

  override def beforeAll() = {
    file = filesHelper.createTempFile(contents = message)
  }

  override def afterAll() = {
    filesHelper.cleanup()
  }

  it must "processes file by multiple chunks correctly" in {
    val reader = Reader(file = file, chunkSize = 4)
    val result = reader.chunks().foldLeft(new StringBuilder)((res, bytes) => res.append(new String(bytes, "UTF-8")))
    result.toString() should be (message)
  }

  it must "processes file by single chunk correctly" in {
    val reader = Reader(file = file)
    val result = reader.chunks().foldLeft(new StringBuilder)((res, bytes) => res.append(new String(bytes, "UTF-8")))
    result.toString() should be(message)
  }
}
