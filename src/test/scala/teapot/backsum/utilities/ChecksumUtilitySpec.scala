package teapot.backsum.utilities

import org.scalatest.{FlatSpec, Matchers}
import teapot.backsum.utilities.ChecksumUtility._

class ChecksumUtilitySpec extends FlatSpec with Matchers {

  behavior of "ChecksumUtility"

  val md5 = "01234567".getBytes
  val sha1 = "0123456789ABCDEFGHIJ".getBytes

  val base64Checksums = "MDEyMzQ1NjcwMTIzNDU2Nzg5QUJDREVGR0hJSg=="

  it must "encode checksums" in {
     encode(md5 = md5, sha1 = sha1) should be (base64Checksums)
  }

  it must "decode checksums" in {
    val decoded = decode(base64Checksums)
    decoded._1 should be (md5)
    decoded._2 should be (sha1)
  }

}
