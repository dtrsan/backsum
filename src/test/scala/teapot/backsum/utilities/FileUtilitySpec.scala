package teapot.backsum.utilities

import org.scalatest.{FlatSpec, Matchers}
import teapot.backsum.helpers.FilesHelper

class FileUtilitySpec extends FlatSpec with Matchers {

  behavior of "FileUtility"

  val filesHelper = new FilesHelper("file-utility")

  it must "get relative path" in {
    val basePath = filesHelper.path("/dir1/subdir1")
    val file = filesHelper.path("/dir1/subdir1/subdir2/file1")
    val relativePath = FileUtility.relativePath(basePath)(_)
    relativePath(file) should be ("subdir2/file1")
  }

  def cwd = System.getProperty("user.dir")
}
