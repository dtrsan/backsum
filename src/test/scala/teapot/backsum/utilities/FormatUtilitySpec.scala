package teapot.backsum.utilities

import org.scalatest.{FlatSpec, Matchers}
import teapot.backsum.utilities.FormatUtility._

class FormatUtilitySpec extends FlatSpec with Matchers {

  behavior of "FormatUtility"

  val message = "The quick brown fox jumps over the lazy dog"
  val hexMessage = "54686520717569636b2062726f776e20666f78206a756d7073206f76657220746865206c617a7920646f67"
  val base64Message = "VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZw=="

  it must "hex encode message" in {
    hexMessage should be (toHex(message.getBytes))
  }

  it must "base64 encode message" in {
    toBase64(message.getBytes) should be (base64Message)
  }

  it must "base64 decode message" in {
    fromBase64(base64Message) should be (message.getBytes)
  }

  it must "throw exception during decoding invalid base64 encoded message" in {
    a [IllegalArgumentException] should be thrownBy {
      fromBase64("==")
    }
  }
}
