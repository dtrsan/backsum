package teapot.backsum.cli

import org.scalatest.{FlatSpec, Matchers}
import teapot.backsum.utilities.FileUtility.toPath
import teapot.configuration._

class ParametersSpec extends FlatSpec with Matchers {

  behavior of "Parameter parser"

  it must "parse only required arguments for compute command" in {
    val args = "compute /path".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isDefined should be (true)

    val expected = Config(
      database = toPath("."),
      database2 = toPath("."),
      exclude = Seq(),
      force = false,
      fullChecksum = false,
      include = Seq(),
      mode = Some(ComputeMode),
      path = toPath("/path"),
      verbose = false,
      version = false
    )

    shouldBeTheSame(maybeConfig.get, expected)
  }

  it must "parse all optional long arguments for compute command" in {
    val args = "compute --include A --include B --exclude C --exclude D --full --force --verbose --output /output /path".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isDefined should be (true)

    val expected = Config(
      database = toPath("/output"),
      database2 = toPath("."),
      exclude = Seq("C".r, "D".r),
      force = true,
      fullChecksum = true,
      include = Seq("A".r, "B".r),
      mode = Some(ComputeMode),
      path = toPath("/path"),
      verbose = true,
      version = false
    )

    shouldBeTheSame(maybeConfig.get, expected)
  }

  it must "parse all optional short arguments for compute command" in {
    val args = "compute --include A --include B --exclude C --exclude D --full -f --verbose -o /output /path".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isDefined should be (true)

    val expected = Config(
      database = toPath("/output"),
      database2 = toPath("."),
      exclude = Seq("C".r, "D".r),
      force = true,
      fullChecksum = true,
      include = Seq("A".r, "B".r),
      mode = Some(ComputeMode),
      path = toPath("/path"),
      verbose = true,
      version = false
    )

    shouldBeTheSame(maybeConfig.get, expected)
  }

  it must "not successfully parse arguments for compute command without required parameter <PATH>" in {
    val args = "compute".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isEmpty should be (true)
  }

  it must "not successfully parse include parameter for compute command when invalid regex pattern is provided" in {
    val args = "compute --include (] /path".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isEmpty should be (true)
  }

  it must "not successfully parse exclude parameter for compute command when invalid regex pattern is provided" in {
    val args = "compute --exclude (] /path".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isEmpty should be (true)
  }

  it must "parse only required arguments for verify command" in {
    val args = "verify /database /path".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isDefined should be (true)

    val expected = Config(
      database = toPath("/database"),
      database2 = toPath("."),
      exclude = Seq(),
      force = false,
      fullChecksum = false,
      include = Seq(),
      mode = Some(VerifyMode),
      path = toPath("/path"),
      verbose = false,
      version = false
    )

    shouldBeTheSame(maybeConfig.get, expected)
  }

  it must "parse all optional long arguments for verify command" in {
    val args = "verify --include A --include B --exclude C --exclude D --verbose /database /path".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isDefined should be (true)

    val expected = Config(
      database = toPath("/database"),
      database2 = toPath("."),
      exclude = Seq("C".r, "D".r),
      force = false,
      fullChecksum = false,
      include = Seq("A".r, "B".r),
      mode = Some(VerifyMode),
      path = toPath("/path"),
      verbose = true,
      version = false
    )

    shouldBeTheSame(maybeConfig.get, expected)
  }

  it must "parse all optional short arguments for verify command" in {
    // no short options
  }

  it must "not successfully parse arguments for verify command without required parameters <DATABASE> and <PATH>" in {
    val args = "verify".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isEmpty should be (true)
  }

  it must "not successfully parse arguments for verify command without required parameter <PATH>" in {
    val args = "verify /database".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isEmpty should be (true)
  }

  it must "not successfully parse include parameter for verfy command when invalid regex pattern is provided" in {
    val args = "verify --include (] /path".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isEmpty should be (true)
  }

  it must "not successfully parse exclude parameter for verify command when invalid regex pattern is provided" in {
    val args = "verify --exclude (] /path".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isEmpty should be (true)
  }
  it must "parse only required arguments for diff command" in {
    val args = "diff /database1 /database2".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isDefined should be (true)

    val expected = Config(
      database = toPath("/database1"),
      database2 = toPath("/database2"),
      exclude = Seq(),
      force = false,
      fullChecksum = false,
      include = Seq(),
      mode = Some(DiffMode),
      path = toPath("."),
      verbose = false,
      version = false
    )

    shouldBeTheSame(maybeConfig.get, expected)
  }

  it must "parse all optional long arguments for diff command" in {
    val args = "diff --verbose /database1 /database2".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isDefined should be (true)

    val expected = Config(
      database = toPath("/database1"),
      database2 = toPath("/database2"),
      exclude = Seq(),
      force = false,
      fullChecksum = false,
      include = Seq(),
      mode = Some(DiffMode),
      path = toPath("."),
      verbose = true,
      version = false
    )

    shouldBeTheSame(maybeConfig.get, expected)
  }

  it must "parse all optional short arguments for diff command" in {
    // no short options
  }

  it must "not successfully parse arguments for diff command without required parameters <DATABASE1> and <DATABASE2>" in {
    val args = "diff".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isEmpty should be (true)
  }

  it must "not successfully parse arguments for diff command without required parameter <DATABASE1>" in {
    val args = "diff /database".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isEmpty should be (true)
  }

  it must "parse version flag" in {
    val args = "--version".split(" ")

    val maybeConfig = Parameters().parse(args)
    maybeConfig.isDefined should be (true)

    val expected = Config(
      database = toPath("."),
      database2 = toPath("."),
      exclude = Seq(),
      force = false,
      fullChecksum = false,
      include = Seq(),
      mode = None,
      path = toPath("."),
      verbose = false,
      version = true
    )

    shouldBeTheSame(maybeConfig.get, expected)
  }

  def shouldBeTheSame(c1: Config, c2: Config) = {
    c1.database should be (c2.database)
    c1.database2 should be (c2.database2)
    c1.exclude.map(_.toString) should be (c2.exclude.map(_.toString))
    c1.force should be (c2.force)
    c1.fullChecksum should be (c2.fullChecksum)
    c1.include.map(_.toString) should be (c2.include.map(_.toString))
    c1.mode should be (c2.mode)
    c1.path should be (c2.path)
    c1.verbose should be (c2.verbose)
    c1.version should be (c2.version)
  }
}
