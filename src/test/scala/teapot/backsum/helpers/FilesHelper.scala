package teapot.backsum.helpers

import java.nio.charset.StandardCharsets
import java.nio.file.attribute.PosixFilePermissions
import java.nio.file.{Files, Path, Paths}

import scala.collection.mutable
import scala.util.Random

class FilesHelper(prefix: String) {

  private val createdPaths = mutable.Stack[Path]()

  val contents: String = "The quick brown fox jumps over the lazy dog"

  val dummyPath: Path = {
    createdPaths.push(Paths.get("/tmp/backsum-" + prefix + "-dummy." +  Random.alphanumeric.take(10).mkString))
    createdPaths.head
  }

  def path(first: String, rest: String*): Path = Paths.get(first, rest:_*)

  def path(dir: Path, filename: String): Path = Paths.get(dir.toString, filename)

  def createTempFile(contents: String = contents, perms: String = "rw-------"): Path = {
    val attrs = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString(perms))
    val file = registerPath(Files.createTempFile("backsum-" + prefix, ".test", attrs))
    Files.write(file, contents.getBytes(StandardCharsets.UTF_8))
  }

  def createTempDir(perms: String = "rwx------"): Path = {
    val attrs = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString(perms))
    registerPath(Files.createTempDirectory("backsum-" + prefix + "-", attrs))
  }

  def createFile(path: Path, contents: String = contents): Path = {
    val file = registerPath(Files.createFile(path))
    Files.write(file, contents.getBytes(StandardCharsets.UTF_8))
  }

  def createDir(path: Path): Path = registerPath(Files.createDirectory(path))

  def createSymLink(link: Path, target: Path): Path = registerPath(Files.createSymbolicLink(link, target))

  def registerPath(path: Path): Path = createdPaths.push(path).head

  def cleanup(): Unit = {
    createdPaths.iterator.foreach(Files.deleteIfExists)
    createdPaths.clear()
  }
}
