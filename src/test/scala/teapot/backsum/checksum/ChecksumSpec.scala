package teapot.backsum.checksum

import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import teapot.backsum.checksum.Checksum.{MD5, SHA1}
import teapot.backsum.utilities.FormatUtility.toHex

class ChecksumSpec extends FlatSpec with Matchers with BeforeAndAfterAll {

  behavior of "Checksum"

  val message = "The quick brown fox jumps over the lazy dog".getBytes
  val hashes = Map(
    "MD5" -> "9e107d9d372bb6826bd81d3542a419d6",
    "SHA-1" -> "2fd4e1c67a2d28fced849ee1bb76e7391b93eb12"
  )

  it must "initialize MD5 message digest" in {
    val checksum = Checksum(MD5)
    toHex(checksum.digest(message)) should be (hashes("MD5"))
  }

  it must "initialize SHA-1 message digest" in {
    val checksum = Checksum(SHA1)
    toHex(checksum.digest(message)) should be (hashes("SHA-1"))
  }

  it must "calculate MD5 message digest correctly" in {
    val checksum = Checksum(MD5)
    checksum.update(message)
    toHex(checksum.digest()) should be (hashes("MD5"))
  }

  it must "calculate SHA-1 message digest correctly" in {
    val checksum = Checksum(SHA1)
    checksum.update(message)
    toHex(checksum.digest()) should be (hashes("SHA-1"))
  }

  it must "reset MD5 message digest" in {
    val checksum = Checksum(MD5)
    checksum.update(message)
    checksum.update(message)
    checksum.reset()
    toHex(checksum.digest(message)) should be (hashes("MD5"))
  }
}
