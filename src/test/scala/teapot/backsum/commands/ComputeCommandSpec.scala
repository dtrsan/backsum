package teapot.backsum.commands

import java.nio.file.Files.getLastModifiedTime
import java.nio.file.Path
import java.util.Date

import org.scalatest.{FlatSpec, Matchers}
import teapot.backsum.commands.ComputeCommand.Checksums
import teapot.backsum.db.Database
import teapot.backsum.exceptions.BacksumIllegalArgumentException
import teapot.backsum.helpers.FilesHelper
import teapot.backsum.models.FileMetadata

import scala.util.matching.Regex

class ComputeCommandSpec extends FlatSpec with Matchers {

  val filesHelper = new FilesHelper("compute-command")
  val contents = filesHelper.contents
  val checksum = "nhB9nTcrtoJr2B01QqQZ1i/U4cZ6LSj87YSe4bt25zkbk+sS"
  val md5sum = "9e107d9d372bb6826bd81d3542a419d6".sliding(2,2).toArray.map(Integer.parseInt(_, 16).toByte)
  val sha1sum = "2fd4e1c67a2d28fced849ee1bb76e7391b93eb12".sliding(2,2).toArray.map(Integer.parseInt(_, 16).toByte)

  behavior of "ComputeCommand"

  it must "throw exception when path does not exist" in {
    val cmd = command()
    val ex = intercept[BacksumIllegalArgumentException] {
      cmd.execute()
    }
    ex.getMessage should be (s"The path '${filesHelper.dummyPath}' does not exist.")
  }

  it must "throw exception when path is not a directory" in {
    val path = filesHelper.createTempFile()
    val cmd = command(path = path)
    val ex = intercept[BacksumIllegalArgumentException] {
      cmd.execute()
    }
    ex.getMessage should be (s"The '$path' is not a directory.")
    filesHelper.cleanup()
  }

  it must "throw exception when output path directory does not exist" in {
    val path = filesHelper.createTempDir()
    val output = filesHelper.path(filesHelper.dummyPath, "db")
    val cmd = command(path = path, outputPath = output)
    val ex = intercept[BacksumIllegalArgumentException] {
      cmd.execute()
    }
    ex.getMessage should be (s"Couldn't create database 'db'. Directory '${filesHelper.dummyPath}' does not exist.")
    filesHelper.cleanup()
  }

  it must "throw exception when output path is not writable" in {
    val path = filesHelper.createTempDir()
    val output = filesHelper.createTempDir(perms = "r-x------")
    val cmd = command(path = path, outputPath = output)
    val ex = intercept[BacksumIllegalArgumentException] {
      cmd.execute()
    }
    ex.getMessage should be (s"Couldn't create database '.backsum.db'. No write permission for '$output'.")
    filesHelper.cleanup()
  }

  it must "throw exception when cannot acquire lock on database" in {
    val path = filesHelper.createTempDir()
    val output = filesHelper.createTempDir()
    val databasePath = filesHelper.registerPath(filesHelper.path(output, "backsum.db"))

    val db = Database(databasePath)
    db.metadataService.lock()
    db.metadataService.isLocked should be (true)
    db.close()

    val cmd = command(path = path, outputPath = databasePath)
    val ex = intercept[BacksumIllegalArgumentException] {
      cmd.execute()
    }
    ex.getMessage should be (s"Couldn't acquire lock on database '$databasePath'.")

    filesHelper.cleanup()
  }

  it must "process a file" in {
    val path = filesHelper.createTempDir()
    val file = filesHelper.createFile(filesHelper.path(path, "file"))
    val databasePath = filesHelper.registerPath(filesHelper.path(filesHelper.createTempDir(), "backsum.db"))

    val cmd = command(path = path, outputPath = databasePath)
    cmd.execute()

    val db = Database(databasePath)
    val metadata = db.filesService.get("file")
    db.close()

    metadata.isDefined should be (true)
    metadata.get.path should be ("file")
    metadata.get.checksum should be (checksum)
    metadata.get.size should be (contents.length)
    metadata.get.modified.getTime should be (getLastModifiedTime(file).toMillis)

    filesHelper.cleanup()
  }

  it must "unlock database when finished" in {
    val path = filesHelper.createTempDir()
    val databasePath = filesHelper.registerPath(filesHelper.path(filesHelper.createTempDir(), "backsum.db"))
    val cmd = command(path = path, outputPath = databasePath)
    cmd.execute()

    val db = Database(databasePath)
    db.metadataService.isLocked should be (false)

    db.close()
    filesHelper.cleanup()
  }

  behavior of "Method process"

  it must "return false when file is not readable" in {
    val cmd = command()
    val file = filesHelper.createTempFile(perms = "-w-------")
    val databasePath = filesHelper.registerPath(filesHelper.path(filesHelper.createTempDir(), "backsum.db"))
    val db = Database(databasePath)

    val result = cmd.process(db.filesService, file.getParent)(file)
    result should be (false)

    db.close()
    filesHelper.cleanup()
  }

  it must "return true when file is processed" in {
    val cmd = command()
    val file = filesHelper.createTempFile()
    val databasePath = filesHelper.registerPath(filesHelper.path(filesHelper.createTempDir(), "backsum.db"))
    val db = Database(databasePath)

    val result = cmd.process(db.filesService, file.getParent)(file)
    result should be (true)

    db.close()
    filesHelper.cleanup()
  }


  behavior of "Method computeChecksums"

  it must "calculate checksums for a file correctly" in {
    val cmd = command()
    val file = filesHelper.createTempFile()
    val checksums = cmd.computeChecksums(file).get

    checksums.md5 should be (md5sum)
    checksums.sha1 should be (sha1sum)
    filesHelper.cleanup()
  }


  behavior of "Method shouldUpdateChecksums"

  it must "return false when all attributes match" in {
    val cmd = command()
    val metadata = fileMetadata()
    val existing = metadata.copy()
    cmd.shouldUpdateChecksums(metadata, Some(existing)) should be (false)
  }

  it must "return true for a new file" in {
    val cmd = command()
    val metadata = fileMetadata()
    cmd.shouldUpdateChecksums(metadata, None) should be (true)
  }

  it must "return true when last modified timestamps do not match" in {
    val cmd = command()
    val metadata = fileMetadata()
    val existing = metadata.copy(modified = new Date(120))
    cmd.shouldUpdateChecksums(metadata, Some(existing)) should be (true)
  }

  it must "return true when sizes do not match" in {
    val cmd = command()
    val metadata = fileMetadata()
    val existing = metadata.copy(size = 6)
    cmd.shouldUpdateChecksums(metadata, Some(existing)) should be (true)
  }

  it must "return true when checksums do not match" in {
    val cmd = command()
    val metadata = fileMetadata()
    val existing = metadata.copy(checksum = "ABC")
    cmd.shouldUpdateChecksums(metadata, Some(existing)) should be (true)
  }


  behavior of "Method shouldComputeChecksums"

  it must "return true when full checksum flag is turned on" in {
    val cmd = command(fullChecksum = true)
    cmd.shouldComputeChecksums(Some(fileMetadata()), filesHelper.dummyPath) should be (true)
  }

  it must "return true when fileMetadata is empty" in {
    val cmd = command()
    cmd.shouldComputeChecksums(None, filesHelper.dummyPath) should be (true)
  }

  it must "return true when last modified timestamps do not match" in {
    val cmd = command()
    val file = filesHelper.createTempFile()
    val metadata = fileMetadata(
      path = file.toString,
      checksum = checksum,
      size = contents.length
    )
    cmd.shouldComputeChecksums(Some(metadata), file) should be (true)
    filesHelper.cleanup()
  }

  it must "return true when file sizes do not match" in {
    val cmd = command()
    val file = filesHelper.createTempFile()
    val metadata = fileMetadata(
      path = file.toString,
      checksum = checksum,
      modified = new Date(0)
    )
    cmd.shouldComputeChecksums(Some(metadata), file) should be (true)
    filesHelper.cleanup()
  }

  it must "return false when file's metadata matches expected metadata" in {
    val cmd = command()
    val file = filesHelper.createTempFile()
    val metadata = fileMetadata(
      path = file.toString,
      checksum = checksum,
      size = contents.length,
      modified = new Date(getLastModifiedTime(file).toMillis)
    )
    cmd.shouldComputeChecksums(Some(metadata), file) should be (false)
    filesHelper.cleanup()
  }

  behavior of "Method createFileMetadata"

  it must "return correct file metadata" in {
    val file = filesHelper.createTempFile()
    val cmd = command(path = file.getParent)
    val metadata = cmd.createFileMetadata(file, Checksums(md5 = md5sum, sha1 = sha1sum))

    metadata.path should be (file.getFileName.toString)
    metadata.checksum should be (checksum)
    metadata.size should be (contents.length)
    metadata.modified.getTime should be (getLastModifiedTime(file).toMillis)

    filesHelper.cleanup()
  }

  def fileMetadata(path: String = filesHelper.dummyPath.toString,
                   checksum: String = "L3RtcC9kdW1teQ==",
                   size: Long = 16,
                   modified: Date = new Date(0)) =
    FileMetadata(path = path, checksum = checksum, size = size, modified = modified)

  def command(path: Path = filesHelper.dummyPath,
              outputPath: Path = filesHelper.dummyPath,
              fullChecksum: Boolean = false,
              include: Seq[Regex] = Seq(),
              exclude: Seq[Regex] = Seq(),
              force: Boolean = false,
              verbose: Boolean = false) =
    new ComputeCommand(path = path, outputPath = outputPath, fullChecksum = fullChecksum, include = include,
      exclude = exclude, force = force, verbose = verbose)
}
