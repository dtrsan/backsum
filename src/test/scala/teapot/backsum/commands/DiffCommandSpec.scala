package teapot.backsum.commands

import java.io.ByteArrayOutputStream
import java.nio.file.{Files, Path, Paths}

import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import teapot.backsum.db.Database
import teapot.backsum.exceptions.BacksumIllegalArgumentException
import teapot.backsum.helpers.FilesHelper

class DiffCommandSpec extends FlatSpec with Matchers with BeforeAndAfterEach {

  behavior of "DiffCommand"

  val nonExistingPath = Paths.get("/non-existing")
  val filesHelper = new FilesHelper("diff_command")

  var database1a: Path = _
  var database1aCopy: Path = _
  var database1b: Path = _
  var database2a: Path = _
  var database2b: Path = _
  var databaseEmpty1: Path = _
  var databaseEmpty2: Path = _

  override def beforeEach(): Unit = {
    val dir = filesHelper.createTempDir()
    database1a = copyDatabase("test-1a.db", dir)
    database1aCopy = duplicateDatabase(database1a, dir)
    database1b = copyDatabase("test-1b.db", dir)
    database2a = copyDatabase("test-2a.db", dir)
    database2b = copyDatabase("test-2b.db", dir)
    databaseEmpty1 = copyDatabase("test-empty.db", dir)
    databaseEmpty2 = duplicateDatabase(databaseEmpty1, dir)
  }

  private def duplicateDatabase(src: Path, dir: Path) = {
    val db = Paths.get(dir.toString, s"copy-${src.getFileName}")
    Files.copy(src, db)
    filesHelper.registerPath(db)
    db
  }

  private def copyDatabase(name: String, dest: Path) = {
    val dbStream = this.getClass.getResourceAsStream(s"/tests/diff_command/$name")
    val db = Paths.get(dest.toString, name)
    Files.copy(dbStream, db)
    filesHelper.registerPath(db)
    db
  }

  override def afterEach(): Unit = {
    filesHelper.cleanup()
  }

  it must "throw exception when path does not exist" in {
    val cmd = command(nonExistingPath, nonExistingPath)
    val ex = intercept[BacksumIllegalArgumentException] {
      cmd.execute()
    }
    ex.getMessage should be (s"The database '$nonExistingPath' does not exist.")
  }

  it must "throw exception when path is not a database file" in {
    val dir = filesHelper.createTempDir()
    val cmd = command(dir, nonExistingPath)
    val ex = intercept[BacksumIllegalArgumentException] {
      cmd.execute()
    }
    ex.getMessage should be (s"The path '$dir' is not a database file.")
  }

  it must "throw exception when cannot acquire lock on a database" in {
    val cmd = command(database1a, database1a)
    val ex = intercept[BacksumIllegalArgumentException] {
      cmd.execute()
    }
    ex.getMessage should be (s"Couldn't acquire lock on database '$database1a'.")
  }

  it must "process a command" in {
    val cmd = command(database1a, database1b)
    val out = new ByteArrayOutputStream
    Console.withOut(out)(cmd.execute())

    val expected = Set(
      "in_both__contents_changed: ERR",
      "only_in_first: DB1",
      "only_in_second: DB2"
    )
    val actual = out.toString("UTF-8").lines.toSet
    actual should be (expected)
  }

  it must "process a command (reversed parameters)" in {
    val cmd = command(database1b, database1a)
    val out = new ByteArrayOutputStream
    Console.withOut(out)(cmd.execute())

    val expected = Set(
      "in_both__contents_changed: ERR",
      "only_in_first: DB2",
      "only_in_second: DB1"
    )
    val actual = out.toString("UTF-8").lines.toSet
    actual should be (expected)
  }

  it must "process a command when files are either in first or second database" in {
    val cmd = command(database2a, database2b)
    val out = new ByteArrayOutputStream
    Console.withOut(out)(cmd.execute())

    val expected = Set(
      "1_file: DB1",
      "2_file: DB1",
      "3_file: DB2",
      "4_file: DB2",
      "5_file: DB1",
      "6_file: DB1",
      "7_file: DB2",
      "8_file: DB2"
    )
    val actual = out.toString("UTF-8").lines.toSet
    actual should be (expected)
  }

  it must "process a command when databases are empty" in {
    val cmd = command(databaseEmpty1, databaseEmpty2)
    val out = new ByteArrayOutputStream
    Console.withOut(out)(cmd.execute())

    val expected = Set()
    val actual = out.toString("UTF-8").lines.toSet
    actual should be (expected)
  }

  it must "process a command when databases are the same" in {
    val cmd = command(database1a, database1aCopy)
    val out = new ByteArrayOutputStream
    Console.withOut(out)(cmd.execute())

    val expected = Set()
    val actual = out.toString("UTF-8").lines.toSet
    actual should be (expected)
  }

  it must "unlock database when finished" in {
    val cmd = command(database1a, database1b)
    cmd.execute()

    val db1 = Database(database1a)
    db1.metadataService.isLocked should be (false)
    db1.close()

    val db2 = Database(database1b)
    db2.metadataService.isLocked should be (false)
    db2.close()

    filesHelper.cleanup()
  }

  private def command(first: Path, second: Path, verbose: Boolean = false) =
    new DiffCommand(first = first, second = second, verbose = verbose)
}
