package teapot.backsum.commands

import org.scalatest.{FlatSpec, Matchers}

class CommandSpec extends FlatSpec with Matchers {

  behavior of "Command"

  it must "cancel command" in {
    val cmd = new Command {
      override def execute(): Unit = {}
    }

    cmd.isRunning should be (true)
    cmd.cancel()
    cmd.isRunning should be (false)
  }
}
