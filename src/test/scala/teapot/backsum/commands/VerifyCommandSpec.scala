package teapot.backsum.commands

import java.nio.file.{Path, Paths}
import java.util.Date

import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import teapot.backsum.db.Database
import teapot.backsum.exceptions.BacksumIllegalArgumentException
import teapot.backsum.helpers.FilesHelper
import teapot.backsum.models.FileMetadata

import scala.util.matching.Regex

class VerifyCommandSpec extends FlatSpec with Matchers with BeforeAndAfterEach {

  behavior of "VerifyCommand"

  private val DATE_MODIFIED: Long = 1477052189938L

  private val filesHelper = new FilesHelper("verify-command")
  private val contents = "The quick brown fox jumps over the lazy dog"

  private var root: Path = _
  private var db: Database = _
  private var testFile2: Path = _
  private var testFile3: Path = _
  private var dbPath = "verify-command.db"
  private var testFile2Path = "file2"
  private var testFile3Path = "onlyOnDisk"

  val metadata1 = FileMetadata(
    path = "onlyInDatabase",
    checksum = "UUnUAwCaE5x+CFQF73YuGji+fRuYHy+2pKCgUkU/iHNz3B/o",
    size = 0,
    modified = new Date(DATE_MODIFIED)
  )

  val metadata2 = FileMetadata(
    path = testFile2Path,
    checksum = "nhB9nTcrtoJr2B01QqQZ1i/U4cZ6LSj87YSe4bt25zkbk+sS",
    size = 43,
    modified = new Date(DATE_MODIFIED)
  )

  val metadata3 = FileMetadata(
    path = testFile3Path,
    checksum = "8a550ec4a21faac74db0f0103b84e529dfe76515",
    size = 10,
    modified = new Date(1477052079219L)
  )

  private def beforeEachDataDependantTest() {
    root = filesHelper.createTempDir()
    db = Database(Paths.get(root.toString, dbPath))
    filesHelper.registerPath(Paths.get(root.toString, dbPath))

    val insert = db.prepareStatement(s"INSERT INTO files(path, checksum, size, modified) VALUES(?, ?, ?, ?)")
    insert.setString(1, metadata1.path)
    insert.setString(2, metadata1.checksum)
    insert.setLong(3, metadata1.size)
    insert.setLong(4, metadata1.modified.getTime)
    insert.execute()
    insert.clearParameters()
    insert.setString(1, metadata2.path)
    insert.setString(2, metadata2.checksum)
    insert.setLong(3, metadata2.size)
    insert.setLong(4, metadata2.modified.getTime)
    insert.execute()
    insert.clearParameters()
    insert.close()

    testFile2 = filesHelper.createFile(Paths.get(root.toString, testFile2Path), contents)
    testFile3 = filesHelper.createFile(Paths.get(root.toString, testFile3Path), "")
  }

  private def afterEachDataDependantTest() {
    db.close()
    filesHelper.cleanup()
  }

 it must "verify if file exists on disk and return false" in {
    beforeEachDataDependantTest()
    val cmd = command()
    cmd.doesExistOnDisk(Paths.get(root.toString, metadata1.path)) should be (false)
    afterEachDataDependantTest()
  }

  it must "verify if file exists on disk and return true" in {
    beforeEachDataDependantTest()
    val cmd = command()
    cmd.doesExistOnDisk(Paths.get(root.toString, metadata2.path)) should be (true)
    afterEachDataDependantTest()
  }

  it must "verify if file sizes are equal and return true" in {
    beforeEachDataDependantTest()
    val cmd = command()
    cmd.areSizesEqual(Paths.get(root.toString, metadata2.path), metadata2.size) should be (true)
    afterEachDataDependantTest()
  }

  it must "verify if file sizes are equal and return false" in {
    beforeEachDataDependantTest()
    val cmd = command()
    cmd.areSizesEqual(Paths.get(root.toString, metadata3.path), metadata3.size) should be (false)
    afterEachDataDependantTest()
  }

  it must "verify if checksums are equal and return true" in {
    beforeEachDataDependantTest()
    val cmd = command()
    cmd.areChecksumsEqual(Paths.get(root.toString, metadata2.path), metadata2.checksum) should be (true)
    afterEachDataDependantTest()
  }

  it must "verify if checksums are equal and return false" in {
    beforeEachDataDependantTest()
    val cmd = command()
    cmd.areChecksumsEqual(Paths.get(root.toString, metadata3.path), metadata3.checksum) should be (false)
    afterEachDataDependantTest()
  }

  it must "throw exception when path does not exist" in {
    val cmd = command(filesHelper.dummyPath)
    val ex = intercept[BacksumIllegalArgumentException] {
      cmd.execute()
    }
    ex.getMessage should be (s"The path '${filesHelper.dummyPath}' does not exist.")
  }

  it must "throw exception when path is not a directory" in {
    val tempPath = filesHelper.createTempFile()
    val cmd = command(tempPath)
    val ex = intercept[BacksumIllegalArgumentException] {
      cmd.execute()
    }
    ex.getMessage should be (s"The '$tempPath' is not a directory.")
    filesHelper.cleanup()
  }

  it must "throw exception when database path directory does not exist" in {
    val path = filesHelper.createTempDir()
    val output = filesHelper.path(filesHelper.dummyPath, "db")
    val cmd = command(path, output)
    val ex = intercept[BacksumIllegalArgumentException] {
      cmd.execute()
    }
    ex.getMessage should be (s"Couldn't create database 'db'. Directory '${filesHelper.dummyPath}' does not exist.")
    filesHelper.cleanup()
  }

  it must "throw exception when cannot acquire lock on database" in {
    root = filesHelper.createTempDir()
    filesHelper.registerPath(Paths.get(root.toString, dbPath))

    db = Database(Paths.get(root.toString, dbPath))
    db.metadataService.lock()
    db.metadataService.isLocked should be (true)
    db.close()

    val cmd = command()
    val ex = intercept[BacksumIllegalArgumentException] {
      cmd.execute()
    }
    ex.getMessage should be (s"Couldn't acquire lock on database '${Paths.get(root.toString, dbPath)}'.")

    filesHelper.cleanup()
  }

  it must "unlock database when finished" in {
    root = filesHelper.createTempDir()
    filesHelper.registerPath(Paths.get(root.toString, dbPath))
    val cmd = command()
    cmd.execute()

    db = Database(Paths.get(root.toString, dbPath))
    db.metadataService.isLocked should be (false)

    db.close()
    filesHelper.cleanup()
  }

  private def command(basePath: Path = root,
              database: Path = Paths.get(root.toString, dbPath),
              include: Seq[Regex] = Seq(),
              exclude: Seq[Regex] = Seq(),
              verbose: Boolean = false) =
              new VerifyCommand(basePath = basePath, databasePath = database, include = include, exclude = exclude, verbose = verbose)

}