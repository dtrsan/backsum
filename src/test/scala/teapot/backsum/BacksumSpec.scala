package teapot.backsum

import java.nio.file.Path

import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import teapot.backsum.commands.{Command, ComputeCommand, DiffCommand, VerifyCommand}
import teapot.backsum.helpers.FilesHelper
import teapot.configuration.{ComputeMode, Config, DiffMode, VerifyMode}

class BacksumSpec extends FlatSpec with Matchers with BeforeAndAfterEach {

  behavior of "Backsum"

  var dbFile1: Path = _
  var dbFile2: Path = _

  val filesHelper = new FilesHelper("backsum")

  override def beforeEach(): Unit = {
    dbFile1 = filesHelper.createTempFile()
    dbFile2 = filesHelper.createTempFile()
  }

  override def afterEach(): Unit = {
    filesHelper.cleanup()
  }

  it must "build compute command" in {
    val config = Config(mode = Some(ComputeMode), database = dbFile1)
    val isRightInstance = Backsum.buildCommand(config).isInstanceOf[ComputeCommand]
    isRightInstance should be (true)
  }

  it must "build verify command" in {
    val config = Config(mode = Some(VerifyMode), database = dbFile1)
    val isRightInstance = Backsum.buildCommand(config).isInstanceOf[VerifyCommand]
    isRightInstance should be (true)
  }

  it must "build diff command" in {
    val config = Config(mode = Some(DiffMode), database = dbFile1, database2 = dbFile2)
    val isRightInstance = Backsum.buildCommand(config).isInstanceOf[DiffCommand]
    isRightInstance should be (true)
  }

  it must "build simple command command to print version" in {
    val config = Config(mode = None, version = true)
    // not really tested
    val isRightInstance = Backsum.buildCommand(config).isInstanceOf[Command]
    isRightInstance should be (true)
  }

  it must "build simple command command to print usage" in {
    val config = Config(mode = None, version = false)
    // not really tested
    val isRightInstance = Backsum.buildCommand(config).isInstanceOf[Command]
    isRightInstance should be (true)
  }
}
